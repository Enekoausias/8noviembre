<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace App\Controllers;
use App\Models\ProductoModel;

/**
 * Description of ProductoController
 *
 * @author Eneko
 */
class ProductoController extends BaseController {
    
        public function ListadoProductos() {
            
        $data['$title'] = 'Listado Productos';
        $productoModel = new ProductoModel();
        $data['productos']=$productoModel->findAll();
        return view('producto/formalta',);
    }
    
    public function InsertProducto() {

    $Color = $this->request->getPost('Color');
    $Nombre = $this->request->getPost('Nombre');
   
    $producto_nuevo = [
            'Color' => $Color,
            'Nombre' => $Nombre,
            ];
      
    $productoModel = new ProductoModel();
    $productoModel->insert($producto_nuevo);
    return redirect()->to('producto/formalta');
    
    }
   
}
